# Getting Started  
The first thing you will want to do to get started is to pull in all the dependencies with:
```
npm install
```

Then you will need three files in the src directory:  
1. guids.json  
This file should just be an array that contains name -> guid for your Electric Forest attendees. To get the guid, you'll need to get it from your lyte.com waiting list link.  
```
[  
    "name": "guid",
    "name": "guid"
] 
```

2. ef.db  
This should just be a SQL database with one table called "queue\_pos". The schema is just: Name (text), Pos(int), Date(text), Guid(text)  

3. slack.json
You only need this file if you plan on using slack\_bot.js it's a super simple json file that just needs the webhook url in it as a property.  
```
{
    "webhookUrl": "https://slack.com"
}
```

Once you have all that set up, go ahead and run:
```
node scraper.js
```
to scrape data into the database.

And:
```
node slack_bot.js
```
To query and publish your scraped data to your slack channel.


# Updates
Below is a list of things I would like to change going forward:
* Use a DAO + ORM for DB interactions as opposed to the QueryBuilder
* Normalize data - Person should probably be its own table that has a foreign key to the position table
* Standardize time: Rather than getting the exact current time the moment the API call gets made down to the millisecond, the scraper should apply the same timestamp for all rows inserted for every DB transaction it makes
