const sqlite3 = require('sqlite3').verbose();

class BaseDbLayer {
    constructor(dbName) {
        this.dbName = dbName;
        this.db = new sqlite3.Database(`./${dbName}`, sqlite3.OPEN_READWRITE, (err) => {
            if(err) {
                console.log(err);
            }
            console.log('connected to sqlite db');
        });
    }

    async executeSQL(sql) {
        return new Promise((resolve, reject) => {
            this.db.run(sql, (err) => {
                if (err) {
                    reject(err.message);
                } else {
                    console.log(`Rows inserted ${this.changes}`);
                    resolve(this.changes);
                }
            });
        });
    }

    querySQL(queryString) {
        return new Promise((resolve, reject) => {
            this.db.all(queryString, [], (err, rows) => {
                if(err) {
                    reject(err);
                }
                resolve(rows);
            });
        });
    }

    close() {
        this.db.close();
    }
}

module.exports = BaseDbLayer;
