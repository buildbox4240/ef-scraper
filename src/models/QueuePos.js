class QueuePos {
    constructor(name, position, guid) {
        this.name = name;
        this.position = position;
        this.date = new Date();
        this.epoch = Date.now();
        this.guid = guid;
    }

    getSQLiteRow() {
        return `
            (\'${this.name}\',
            ${this.position},
            \'${this.date}\',
            ${this.epoch},
            \'${this.guid}\')
        `
    }
}

module.exports = QueuePos;
