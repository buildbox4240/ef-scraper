const fetch = require("node-fetch");
const guids = require("./guids.json");
const slackConf = require("./slack.json");
const slackWebhookUrl = slackConf['webhookUrl'];

const sqlite3 = require('sqlite3').verbose();
const dbName = 'ef.db';
const tableName = 'queue_pos';


let db = new sqlite3.Database(`./${dbName}`);
let sql = `SELECT Name, Pos, epoch, max(epoch) FROM ${tableName} GROUP BY Name ORDER BY Pos`;

var message = "Current queue positions: \n";

db.all(sql, [], (err, rows) => {
    if (err) {
        throw err;
    }
    rows.forEach((row) => {
        let rowText = `${row.Name} | ${row.Pos}`
        message = message + rowText + "\n"
    });
    console.log(message);
    var slackMessage = {
        "text": message
    }
    fetch(slackWebhookUrl, { method: 'POST', body: JSON.stringify(slackMessage) })
        .then(res => res.text())
        .then(text => console.log(text));
});

db.close();
