const BaseDbLayer = require("./BaseDbLayer");
const dbName = 'ef.db';
const queuePosTableName = 'queue_pos';

class EfDbLayer {
    constructor() {
        this.db = new BaseDbLayer(dbName);
    }

    getQueryBuilder(tableName) {
        return new QueryBuilder(tableName)
    }

    async insertManyQueuePositions(queuePositions) {
        let sql = `INSERT INTO ${queuePosTableName}('Name', 'Pos', 'Date', 'epoch', 'Guid') VALUES`

        queuePositions.forEach((queuePosition) => {
            sql += queuePosition.getSQLiteRow() + ',';
        });
        sql = sql.substring(0, sql.length - 1);

        return this.db.executeSQL(sql);
    }

    async query(queryString) {
        return this.db.querySQL(queryString);
    }

    finish() {
        this.db.close();
    }
}

class QueryBuilder {
    constructor(tableName) {
        this.tableName = tableName;
        this.whereClauses = [];
        this.fields = [];
        this.groupByField;
        this.orderByField;
    }

    select(fields) {
        this.fields = fields;
        return this;
    }

    with(field, operator, value) {
        this.whereClauses.push({
            "field": field,
            "value": value,
            "operator": operator,
        });
        return this;
    }

    groupBy(fieldName) {
        this.groupByField = fieldName;
        return this;
    }

    orderBy(fieldName) {
        this.orderByField = fieldName;
    }

    build() {
        let query = "";
        console.log(this.fields);
        console.log(this.whereClauses);
        query = `SELECT ${this.fields.join(',')} FROM ${this.tableName}`;

        if(this.whereClauses.length > 0) {
            var whereClause = " WHERE";
            this.whereClauses.forEach((whereClauseObj, index) => {
                whereClause += ` ${whereClauseObj.field} ${whereClauseObj.operator} ${whereClauseObj.value} `
                if(index != this.whereClauses.length - 1) {
                    whereClause += "AND ";
                }
            });
            query += whereClause;
        }

        if(this.groupByField) {
            query += `GROUP BY ${this.groupByField} `;
        }

        if(this.orderByField) {
            query += ` ORDER BY ${this.orderByField} `;
        }

        return query;
    }
}

module.exports = EfDbLayer;
