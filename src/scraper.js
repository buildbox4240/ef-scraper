const guids = require("./guids.json");
const fetch = require("node-fetch");
const QueuePos = require("./models/QueuePos");
const EfDbLayer = require("./EfDbLayer");

const baseUrl = "https://lyte.com/consumer/exchange/request/";

async function main() {
    var dataToInsert = [];
    for(const nameGuidPair of guids) {
        let res = await fetch(baseUrl + nameGuidPair.guid)
        let resJson = await res.json();
        var userPosition = new QueuePos(nameGuidPair.name, resJson.data.queue_position, nameGuidPair.guid);
        dataToInsert.push(userPosition);
    }
    console.log(dataToInsert);
    var dbLayer = new EfDbLayer();
    var insertResponse = await dbLayer.insertManyQueuePositions(dataToInsert);
    dbLayer.finish();
}

main()
